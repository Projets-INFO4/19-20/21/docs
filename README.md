**Informations utiles :**

**Lien du Wiki :** https://air.imag.fr/index.php/Projets_2019-2020

**Lien Drive (Tableau de Bord) :** https://docs.google.com/document/d/1-tSghENboJ8BV2c5Yi7ZzLaITkaBaVjNpRu26ZTeE0Y/edit?fbclid=IwAR1kEyGfbJlh2cn5_59pb85kU7edlNOWs2EJmyXDUmkSW3Gg4lrFqUUPXsw

**Lien Doc Mimtmproxy :** https://docs.mitmproxy.org/stable/

**Lien Source Cproxy :** https://github.com/coiby/cproxy

**Lien Doc Kameleon :** http://kameleon.imag.fr/

**Lien Diapo Présentation Mi-parcours :** https://docs.google.com/presentation/d/1vudyMUDkvEGZWSEPNK446b4Mo4z7vZkM4i-2NNehgmo/edit?usp=sharing


---------------------------------------------------------------------------------------------------------------------------------------

***Tableau de Bord Projet “Proxy Cache HTTPS”
Raphaël Audin / Gaëtan Rival 2019-2020***

---------------------------------------------------------------------------------------------------------------------------------------

**Semaine 1 (27 janvier - 2 février)**

*1er réunion avec M. Richard :* Discussion à propos du sujet, des attentes et de l’organisation

*  Preuve du concept attendu (HTTP et HTTPS)

*  Tester la solution pour ensuite l’intégrer dans un outil (Ruby)

*  Principe du “Man of Middle”  → Logiciel “Mimtmproxy”

*  S’informer sur les certificats (autorité) à propos de Curl/Wget

*  Langage Python et Ruby éventuellement nécessaire pour effectuer les tests 

*  Autre solution possible avec le Cproxy, se renseigner et voir son fonctionnement

*  Débuter preuve de concept → cache qui pendant que je construis tout ce qui vient de réseau et on exécute ce qui est dans le cache

**|Aide|**

Accèder à 1 site par proxy, couper la liaison extérieure, rejouer wget et cela doit fonctionner tout comme en htpps

Comment faire du cache https → librairie ligne de commande
Lien intéressant: https://borntocode.fr/mitmproxy-analyser-le-trafic-de-vos-applications-mobiles

**DID :**
* Installation de Mimtmproxy

* Commencement de la prise en main du logiciel

* Renseignements sur le principe du cache HTPPS et Man of Middle

**TODO :**
* Continuer à se renseigner sur le logiciel Mimtmproxy

* Premiers tests simples à mettre en place

* 2eme réunion avec M. Richard

---------------------------------------------------------------------------------------------------------------------------------------
**Semaine 2 (3 février - 9 février)**

**DID :**
* 2eme réunion avec M. Richard pour effectuer un bilan sur la compréhension du sujet et voir notre avancé

* Renseignements / Installation / Tests Squid

* Continuer à se renseigner sur le logiciel Mimtmproxy

* Premiers petits tests de compréhension

**TODO :**

* Prolonger la connaissance de Mimtproxy

* Développer les premiers tests autour de Mimtproxy

* Approfondir l'idée de Squid

---------------------------------------------------------------------------------------------------------------------------------------
**Semaine 3 (10 février - 16 février)**

**DID :**
* Test Mimtproxy et développement petits Scripts de tests
 
* Mise en place de Squid et développement de l'idée

**TODO :**
* Installation et compréhension de Cproxy

* 3eme réunion pour un bilan


---------------------------------------------------------------------------------------------------------------------------------------
**Semaine 4 (17 février - 23 février)**

3eme réunion avec M. Richard : Clarification du sujet + point sur l’avancé du projet

**DID :** 
* SQUID : Serveur fonctionnel, cache OK pour requête en HTTP

* Mitmproxy : Enregistrement des requêtes dans un fichier et replay du côté client

* Installation et compréhension de Cproxy

* Premiers petits tests de compréhension combinant Mitmproxy et Cproxy


**TODO :** 
* SQUID : Voir pour requêtes HTTPS en installant l'autorité de certification

* Préparer présentation oral

* Développer les premiers tests autour de Mimtproxy et Cproxy

* Faire un schéma des étapes d'autocertif pour pré-soutenance

---------------------------------------------------------------------------------------------------------------------------------------
**Semaine 5 (24 février - 1 mars)**

**DID**
* Préparation soutenance orale avec diaporama

* Controle des tests Cproxy + Mitmproxy (Problème de comptabilité)

* Autorité de certification HTTPS sur SQUID

**TODO**
* Creuser le problème entre Cproxy et Mitmproxy

* Finalisation SQUID

---------------------------------------------------------------------------------------------------------------------------------------
**Semaine 6 (2 mars - 8 mars)**

4eme réunion avec M. Richard : Point sur l’avancé du projet + Explication preuve d'autocertification

**DID**
* Préparation soutenance orale avec diaporama

* Installation et début de compréhension de l'outil "Kameleon"

**TODO**
* Prolonger la compréhension avec Kameleon utilisant Polipo

* Ensuite remplacer Polipo par Cproxy pour mettre en place le cache HTTPS

---------------------------------------------------------------------------------------------------------------------------------------
**Semaine 7 (9 mars - 15 mars)**

9/03: Soutenance de mi-parcours

**DID**
* Finalisation de la preuve de concept de Squid

* Préparation de la soutenance

* Modification du SRS et upload

* Lecture de documentation/tuto sur kameleon et sa mise en place

**TODO**
* Configuration de kameleon

---------------------------------------------------------------------------------------------------------------------------------------
**Semaine 8 (16 mars - 22 mars)**

Interruption des enseignements en raison du COVID-19

* Communication des encadrants sur la continuité pédagogique

* Mise en place des procédures de travail à distance

* Point sur le travail à réaliser

---------------------------------------------------------------------------------------------------------------------------------------
**Semaine 9 (23 mars - 29 mars)**

**DID**
* Manipulation Kameleon avec les recettes "test"


**Error / difficulties**
* Integrer Cproxy dans le fichier persistant_cache

* Tester les fonctionnalités Cproxy sur les machines virtuelles


**TODO**
* Continuer les manipulations Kameleon

---------------------------------------------------------------------------------------------------------------------------------------
**Semaine 10 (30 mars - 5 avril)**

**DID**
* Manipulation Kameleon avec les recettes "test"


**TODO**
* Continuer les manipulations Kameleon

---------------------------------------------------------------------------------------------------------------------------------------
**Semaine 11 (6 avril - 12 avril)**

**DID**
* Manipulation Kameleon avec les recettes "test"


**TODO**
* Continuer les manipulations Kameleon

---------------------------------------------------------------------------------------------------------------------------------------
**Semaine 12 (13 avril - 19 avril)**

**DID**
* Insertion Cproxy dans Kaméleon mais ne fonctionne pas encore car présente des problèmes d'adaptations

**TODO**
* Approfondir les problemes d'adaptations entre Cproxy et Kameleon

* Continuer les manipulations Kameleon

* Rapport

Remarque: en attente de consignes sur le déroulement de la soutenance 

---------------------------------------------------------------------------------------------------------------------------------------
**Semaine 13 (20 avril - 26 avril)**


**DID**
* Insertion Cproxy dans Kaméleon mais ne fonctionne pas encore car présente des problèmes d'adaptations

* Début rapport


**TODO**
* Approfondir les problemes d'adaptations entre Cproxy et Kameleon

* Continuer les manipulations Kameleon

* Rapport

---------------------------------------------------------------------------------------------------------------------------------------
**Semaine 14 (27 avril - 30 avril)**

* Rapport de fin de projet

* Mis à jour du wiki